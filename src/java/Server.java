import java.io.IOException;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Server {
    private static Map<String, MyConnection> connectionMap = new ConcurrentHashMap<>();

    private static class Handler extends Thread {
        @Override
        public void run() {
            String userName;
            try {
                MyMessageProvider provider= new MyMessageProvider();
                MyMessageConsumer consumer = new MyMessageConsumer();
                ConsoleHelper.writeMessage("установлено новое соединение");
                userName = serverHandshake(provider,consumer);
                //serverMainLoop(connection, userName);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        private Socket socket;

        public Handler() {
        }

        private String serverHandshake(MyMessageProvider provider, MyMessageConsumer consumer) throws IOException, ClassNotFoundException {
            //while (true) {
                MyMessage myMessage_request = new MyMessage(MessageType.NAME_REQUEST);
                provider.send(myMessage_request);
                MyMessage myMessage_response = consumer.receive();
                if (myMessage_response.getType() == MessageType.USER_NAME) {
                    String name = myMessage_response.getData();
                    if (name != null && !name.isEmpty()) {
                       /* if (!connectionMap.containsKey(name)) {
                            connectionMap.put(name, connection);
                            MyMessage myMessage = new MyMessage(MessageType.NAME_ACCEPTED);
                            connection.send(myMessage);
                            return name;
                        }*/
                    }
              }
            //}
            return "";
        }

/*        private void serverMainLoop(MyConnection connection, String userName) throws IOException, ClassNotFoundException {
            while (true) {
                MyMessage myMessage = connection.receive();
                sendBroadcastMessage(new MyMessage(MessageType.TEXT,
                        String.format("%s: %s", userName, myMessage.getData())));
            }
        }*/
    }

    public static void sendBroadcastMessage(MyMessage myMessage) {
        for (Map.Entry<String, MyConnection> pair : connectionMap.entrySet()) {
                pair.getValue().send(myMessage);
        }
    }

    public static void main(String[] args) {
        System.out.println("Server run");
        //while (true) {
            Handler handler = new Handler();
            handler.start();
        //}

    }

}
