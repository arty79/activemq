

import java.io.Serializable;


public class MyMessage implements Serializable
{
    private final MessageType type;

    private final String data;


    public MessageType getType()
    {
        return type;
    }

    public String getData()
    {
        return data;
    }

    public MyMessage(MessageType type)
    {
        this.type = type;
        this.data = null;
    }

    public MyMessage(MessageType type, String data)
    {
        this.type = type;
        this.data = data;
    }
}
