
import java.io.IOException;


public class Client {
    public MyConnection myConnection;
    private volatile boolean clientConnected;

    public class SocketThread extends Thread {

        public void processIncomingMessage(String message) {
            ConsoleHelper.writeMessage(message);
        }


        public void notifyConnectionStatusChanged(boolean clientConnected) {
            Client.this.clientConnected = clientConnected;
            synchronized (Client.this) {
                Client.this.notify();
            }
        }

        public void clientHandshake() throws IOException, ClassNotFoundException {
            MyMessage message;
            //while (true) {
                message = myConnection.receive();
                if (message.getType().equals(MessageType.NAME_REQUEST)) {
                    myConnection.send(new MyMessage(MessageType.USER_NAME, getUserName()));
                } else if (message.getType().equals(MessageType.NAME_ACCEPTED)) {
                    notifyConnectionStatusChanged(true);
                    return;
                } else throw new IOException("Unexpected MessageType");
            //}
        }

        public void clientMainLoop() throws IOException, ClassNotFoundException {
            MyMessage message;
            //while (true) {
                message = myConnection.receive();
                processIncomingMessage(message.getData());
            //}
        }

        public void run() {
            try {
                myConnection = new MyConnection();
                clientHandshake();
                clientMainLoop();
            } catch (IOException e) {
                notifyConnectionStatusChanged(false);
            } catch (ClassNotFoundException e) {
                notifyConnectionStatusChanged(false);
            }
        }
    }

    public void run() {
        SocketThread socketThread = getSocketThread();
        socketThread.setDaemon(true);
        socketThread.start();
        synchronized (this) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
        }
        if (clientConnected) {
            ConsoleHelper.writeMessage("Соединение установлено.");
        } else {
            ConsoleHelper.writeMessage("Произошла ошибка.");
        }
        while (clientConnected) {
            sendTextMessage(ConsoleHelper.readString());
        }
    }

    public static void main(String[] args) {
        Client client = new Client();
        client.run();
    }

    public String getUserName() {
        ConsoleHelper.writeMessage("Введите имя");
        return ConsoleHelper.readString();
    }

    public SocketThread getSocketThread() {
        return new SocketThread();
    }

    public void sendTextMessage(String text) {
        myConnection.send(new MyMessage(MessageType.TEXT, text));
    }
}
